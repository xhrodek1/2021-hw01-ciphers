package cz.muni.fi.pb162.hw01.impl.ciphers;

/**
 * Class for encrypting and decrypting using Ceaser cipher
 * @author Gorazd
 */
public class CeaserCipher implements Cipher {
    private final StringBuilder cipher;

    /**
     * Constructor for ceaser cipher
     * @param shift of alphabet
     */
    public CeaserCipher(int shift) {
        cipher = shiftAlphabet(shift);
    }

    /**
     *
     * @param shift of alphabet
     * @return shifted alphabet based on shift parameter
     */
    public static StringBuilder shiftAlphabet(int shift){
        return new StringBuilder(ALPHABET.substring(shift)).append(ALPHABET.substring(0,shift));
    }

    /**
     * Shifts string of characters from alphabet to cipher
     * @param text that is supposed to be shifted
     * @param alphabet original char layout
     * @param cipher desired char layout
     * @return shifted text
     */
    private StringBuilder shifter(StringBuilder text, StringBuilder alphabet, StringBuilder cipher){
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            int index = alphabet.indexOf(text.substring(i,i+1));
            if (index>=0){
                result.append(cipher.charAt(index));
            }else{
                result.append(text.charAt(i));
            }

         }
        return result;
    }

    /**
     * Encrypts given plain text using this cipher
     *
     * @param plainText text to be encrypted
     * @return encrypted (cipher) text
     */
    @Override
    public String encrypt(String plainText) {
        return shifter(new StringBuilder(plainText),ALPHABET, cipher).toString();
    }

    /**
     * Decrypts given cipher text using this cipher
     *
     * @param cipherText text to be decrypted
     * @return original (plain) text
     */
    @Override
    public String decrypt(String cipherText) {
        return shifter(new StringBuilder(cipherText), cipher, ALPHABET).toString();
    }
}
