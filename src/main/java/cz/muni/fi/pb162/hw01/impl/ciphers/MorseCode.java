package cz.muni.fi.pb162.hw01.impl.ciphers;

import java.util.HashMap;
import java.util.Map;
/**
 * Class for encrypting and decrypting using Morse code
 * @author Gorazd
 */
public class MorseCode implements Cipher{
    private final Map<String, String> morseMap = new HashMap<>();
    private final Map<String, String> alphaMap = new HashMap<>();

    /**
     * Constructor creating the morseMap
     */
    public MorseCode() {
        String[] alpha = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
                "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
                "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
                ",", ".", "?", "!",};
        for (int i = 0; i < alpha.length; ++i) {
            String[] morse = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..",
                    ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.",
                    "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----",
                    "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.",
                    "-----", "--..--", ".-.-.-", "..--..", "..--.",};
            morseMap.put(alpha[i], morse[i]);
            alphaMap.put(morse[i], alpha[i]);
        }
    }

    /**
     * Encrypts given plain text using this cipher
     *
     * @param plainText text to be encrypted
     * @return encrypted (cipher) text
     */
    @Override
    public String encrypt(String plainText) {
        StringBuilder result = new StringBuilder();

        for (char current: plainText.toLowerCase().toCharArray()) {
            if (current == ' '){
                result.append("|");
            } else if (morseMap.containsKey(String.valueOf(current))){
                result.append(morseMap.get(String.valueOf(current))).append('|');
            } else{
                result.append(current);
            }
        }
        return result.toString();
    }

    /**
     * Decrypts given cipher text using this cipher
     *
     * @param cipherText text to be decrypted
     * @return original (plain) text
     */
    @Override
    public String decrypt(String cipherText) {
        StringBuilder result = new StringBuilder();

        for (String str:cipherText.replace("||","| |").split("\\|")){
            if (morseMap.containsValue(str)){
                result.append(alphaMap.get(str));
            }else{
                result.append(str);
            }
        }
        return result.toString();
    }
}
