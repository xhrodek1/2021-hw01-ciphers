package cz.muni.fi.pb162.hw01.impl.ciphers;
/**
 * Class for encrypting and decrypting using Vigenere cipher
 * @author Gorazd
 */
public class VigenereCipher implements Cipher {
    private final StringBuilder[] keyChain;

    /**
     * constructor creating array of shifted alphabets based on key
     * @param key of shifted alphabets
     */
    public VigenereCipher(String key) {
        int shift;
        keyChain = new StringBuilder[key.length()];
        for (int i = 0; i < key.length(); i++) {
            shift = ALPHABET.indexOf(key.substring(i,i+1));
            if (shift > 0){
                keyChain[i] = CeaserCipher.shiftAlphabet(shift);
            }
        }

    }


    /**
     * Encrypts given plain text using this cipher
     *
     * @param plainText text to be encrypted
     * @return encrypted (cipher) text
     */
    @Override
    public String encrypt(String plainText) {
        StringBuilder result = new StringBuilder();
        int index;
        for (int i = 0; i < plainText.length(); i++) {
            index = ALPHABET.indexOf(plainText.substring(i,i+1));
            if (keyChain[i%keyChain.length] != null  && index >=0) {
                result.append(keyChain[i%keyChain.length].charAt(index));
            } else {
                result.append(plainText.charAt(i));
            }

        }
        return result.toString();
    }

    /**
     * Decrypts given cipher text using this cipher
     *
     * @param cipherText text to be decrypted
     * @return original (plain) text
     */
    @Override
    public String decrypt(String cipherText) {
        StringBuilder result = new StringBuilder();
        int index;
        for (int i = 0; i < cipherText.length(); i++) {
            if (keyChain[i%keyChain.length] != null) {
                index = keyChain[i % keyChain.length].indexOf(cipherText.substring(i, i + 1));
            } else {
                index = -1;
            }
            if (index >=0){
                result.append(ALPHABET.charAt(index));
            } else {
                result.append(cipherText.charAt(i));
            }

        }
        return result.toString();
    }
}
